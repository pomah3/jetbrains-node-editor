# Node editor

## Build

You need maven to build this project

```
mvn compile
mvn assembly:single
mv target/node_editor-1.0-SNAPSHOT-jar-with-dependencies.jar node_editor.jar
```

Or you can visit [Releases page](https://gitlab.com/pomah3/jetbrains-node-editor/-/releases) to download jar.

## Usage

Start:

```
java -jar node_editor.jar <file name>
```

If file doesn't exist, program will create it

Program visualizes tree as nested list:

```
 - (0) (root)  <=====
   - (1)  2
     - (2)  2.1
       - (3)  2.1.1
       - (4)  2.1.2
       - (5)  2.1.3
         - (6)  2.1.3.1
         - (7)  2.1.3.2
       - (8)  2.1.4
     - (9)  2.2
   - (10)  3
```

`<====` marks current node. You can `insert`, `remove` or `choose` current node. `exit` command will save tree to file and exit from program.