package tk.semenov.node_editor;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        if (args.length != 1) {
            printHelp();
            return;
        }
        
        String savingFileName = args[0];
        CliHandler cli = new CliHandler(savingFileName);

        cli.run();
    }

    public static void printHelp() {
        System.out.println(
            "Usage: editor <path-to-file>"
        );
    }
}
