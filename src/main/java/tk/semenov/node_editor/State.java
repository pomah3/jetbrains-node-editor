package tk.semenov.node_editor;

import java.util.ArrayList;
import java.util.HashMap;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


class State {
    private Node root;
    private Node chosen;

    private ArrayList<Node> nodeByNum = new ArrayList<Node>();
    private HashMap<Node, Integer> numByNode = new HashMap<Node, Integer>();

    public State() {
        root = new Node("(root)");
        chosen = root;
        recalcNums();
    }

    private void recalcNums() {
        nodeByNum.clear();
        numByNode.clear();
        recalcNums(root);    
    }
    
    private void recalcNums(Node node) {
        nodeByNum.add(node);
        numByNode.put(node, nodeByNum.size() - 1);

        for (Node child: node.getChilds()) {
            child.setParent(node);
            recalcNums(child);
        }
    }

    public int getNumByNode(Node node) {
        return numByNode.get(node);
    }

    public Node getNodeByNum(int num) {
        return nodeByNum.get(num);
    }

    public void insert(String value) {
        chosen.addChild(new Node(value));
        recalcNums();
    }

    public boolean remove() {
        if (chosen == root) {
            return false;
        } else {
            Node parent = chosen.getParent();
            parent.removeChild(chosen);
            chosen = parent;

            recalcNums();

            return true;
        }
    }

    public boolean choose(int i) {
        if (i < 0 || i >= nodeByNum.size()) 
            return false;

        chosen = nodeByNum.get(i);
        return true;
    }

    public Node getChosen() {
        return chosen;
    }

    public Node getRoot() {
        return root;
    }

    public void saveTo(File file) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        try {
            BufferedWriter fw = new BufferedWriter(new FileWriter(file));
            gson.toJson(root, fw);
            fw.close();
        } catch (IOException exception) {
            System.out.println("Failed to save tree");
        }
    }

    public void loadFrom(File file) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            root = new Gson().fromJson(bufferedReader, Node.class);
            chosen = root;
            recalcNums();
        } catch (IOException exception) {
            System.out.println("Failed to load tree");
        }
    }
}