package tk.semenov.node_editor;

import java.util.List;
import java.util.ArrayList;
import com.google.gson.annotations.Expose;

class Node {
    @Expose
    private final String value;
    @Expose
    private List<Node> nodes;

    private Node parent;

    public Node(String value) {
        this.value = value;
        this.nodes = new ArrayList<Node>();
    }
    
    public Node(String value, Node parent) {
        this.value = value;
        this.nodes = new ArrayList<Node>();
        this.parent = parent;
    }

    public String getValue() {
        return value;
    }

    public void addChild(Node node) {
        nodes.add(node);
        node.parent = this;
    }
    
    public void removeChild(Node node) {
        nodes.remove(node);
        node.parent = null;
    }

    public List<Node> getChilds() {
        return nodes;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node node) {
        parent = node;
    }
}