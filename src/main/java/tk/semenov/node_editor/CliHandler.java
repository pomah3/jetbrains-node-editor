package tk.semenov.node_editor;

import java.io.IOException;
import java.util.Scanner;
import java.util.InputMismatchException;
import java.io.File;

class CliHandler {
    private State state;
    private boolean running;
    private Scanner scanner;
    private String status = "";
    private File savingFile;

    public CliHandler(String savingFileName) {
        state = new State();
        scanner = new Scanner(System.in);

        savingFile = new File(savingFileName);
        if (savingFile.isFile()) {
            state.loadFrom(savingFile);
        }
    }

    public void run() {
        running = true;

        while (running) {
            // Clear console
            System.out.print("\033[H\033[2J");  
            System.out.flush();  


            printTree();
            printHelp();
            printStatus();
            printPrompt();

            String command = scanner.next();

            if ("choose".equals(command)) {
                choose();
            } else if ("insert".equals(command)) {
                insert();
            } else if ("remove".equals(command)) {
                remove();
            } else if ("exit".equals(command)) {
                exit();
            }
        }
    }

    private void printTree() {
        printTree(state.getRoot(), 0);
    }

    void printTree(Node node, int tabs) {
        for (int i=0; i<tabs; i++) {
            System.out.print("  ");
        }

        System.out.print(" - ");
        System.out.print("(");
        System.out.print(state.getNumByNode(node));
        System.out.print(") ");
        System.out.print(node.getValue());
        
        if (state.getChosen() == node)
            System.out.print("  <=====");
        
        System.out.println();
        
        for (Node child: node.getChilds()) {
            printTree(child, tabs+1);
        }
    }

    void printHelp() {
        System.out.println();
        System.out.println("Help: choose <i>  |  insert <value>  |  remove  |  exit");
        System.out.println();
        System.out.println();
    }

    void printStatus() {
        System.out.println(status);
        status = "";
    }

    void printPrompt() {
        System.out.print("(");
        System.out.print(state.getNumByNode(state.getChosen()));
        System.out.print(") > ");
    }

    void choose() {
        try {
            int i = scanner.nextInt();
            state.choose(i);
        } catch (InputMismatchException exception) {}
    }

    void insert() {
        String data = scanner.nextLine();
        state.insert(data);
    }

    void remove() {
        state.remove();
    }

    void exit() {
        running = false;
        state.saveTo(savingFile);
    }
}